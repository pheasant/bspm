# BSPM

## Abstract
A brain-computer interface (BCI) can be defined as a system that translates the brain activity patterns of a user into messages or commands for an interactive application. So, in general, we can define a Brain-Computer Interface (BCI) as a direct communication pathway and control channel between a brain and an external device.
The design of EEG-based BCI consists of a closed-loop interaction between the device and the user’s brain. Every BCI contains a signal acquisition module to measure task-related to brain activity. The row signals are acquired through the EEG electrodes. In our case, the acquisition of the row data is done by an opposite device called "emotiv EPOC+ 14 channel mobile EEG”: AF3, F7, F3, FC5, T7, P7, 01, 02, P8, T8, FC6, F4, F8, AF4.
Our approach is based on the detection of increased average alpha power as a consequence of the closing of the eyes. The classifier (LDA) distinguish between the two states based on the power in the alpha band.
The communication with the track bot (Arduino) was fulfilled by http requests on the same network.

## Requirements
- emotiv EPOC+
- Python>=3.5 with `numpy` and `sklearn`
- EDK library>=3.3 [ https://github.com/Emotiv/community-sdk ]

## Usage
### Acquire data
After the successful installation of emotiv EPOC+ headset, change the value of the variable `EDKPATH` in `take_relax.py` and `take_attention.py` with the path where is the EDK library `edk.dll`. Afterwards, run the file:
`python take_data.py <NAME_FILE>`
### Check model
To run a cross-validation with the acquired data, modifying the file `clean_classify.py` (changing the variables of the data in the main function) and run it.
### Test
The script to move the track bot is `start.py`. In case there is no track bot and just the headset, modifying (changing the variables of the data in the load function) and run:
`python small_test.py`