import sys
import os
import platform
import time
import ctypes
from datetime import datetime

from array import *
from ctypes import *
# from __builtin__ import exit

if sys.platform.startswith('win32'):
    import msvcrt
elif sys.platform.startswith('linux'):
    import atexit
    from select import select

from ctypes import *


if len(sys.argv) != 2:
    print('Usage: python take_data.py <NAME_FILE>')
    exit()

""" Import library """
EDKPATH = "../../../Emotiv SDK Premium Edition v3.3.3/EDK/x86/edk.dll"
try:
    libEDK = cdll.LoadLibrary(EDKPATH)
except Exception as e:
    print('Error: cannot load EDK lib:', e)
    exit()

""" Set variables for connections and data """
IEE_EmoEngineEventCreate = libEDK.IEE_EmoEngineEventCreate
IEE_EmoEngineEventCreate.restype = c_void_p
eEvent = IEE_EmoEngineEventCreate()

IEE_EmoEngineEventGetEmoState = libEDK.IEE_EmoEngineEventGetEmoState
IEE_EmoEngineEventGetEmoState.argtypes = [c_void_p, c_void_p]
IEE_EmoEngineEventGetEmoState.restype = c_int

IEE_EmoStateCreate = libEDK.IEE_EmoStateCreate
IEE_EmoStateCreate.restype = c_void_p
eState = IEE_EmoStateCreate()

userID = c_uint(0)
user   = pointer(userID)
ready  = 0
state  = c_int(0)

alphaValue     = c_double(0)
low_betaValue  = c_double(0)
high_betaValue = c_double(0)
gammaValue     = c_double(0)
thetaValue     = c_double(0)

alpha     = pointer(alphaValue)
low_beta  = pointer(low_betaValue)
high_beta = pointer(high_betaValue)
gamma     = pointer(gammaValue)
theta     = pointer(thetaValue)

secondsLimit = 4

""" Set which channels """
channelList = array('I',[*range(3,17)])

# -------------------------------------------------------------------------
print("===================================================================")
print("Example to get the average band power for a specific channel from the latest epoch.")
print("===================================================================")

# -------------------------------------------------------------------------
if libEDK.IEE_EngineConnect(create_string_buffer(b"Emotiv Systems-5")) != 0:
    print("Emotiv Engine start up failed.")
    exit();

print("BE RELAX !!!!!!!!!! \n  \n")
print("Theta, Alpha, Low_beta, High_beta, Gamma \n")

""" Start to record """
time.sleep(0.01)
start_time = datetime.now()
print(start_time)
while (1):
    state = libEDK.IEE_EngineGetNextEvent(eEvent)
    if state == 0:
        eventType = libEDK.IEE_EmoEngineEventGetType(eEvent)
        libEDK.IEE_EmoEngineEventGetUserId(eEvent, user)
        if eventType == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
            ready = 1
            libEDK.IEE_FFTSetWindowingType(userID, 1);  # 1: libEDK.IEE_WindowingTypes_enum.IEE_HAMMING
        if ready == 1:
            for i in channelList:
                result = c_int(0)
                result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)
                # result = libEDK.IEE_MotionDataGetMultiChannels(userID, i, theta, alpha, low_beta, high_beta, gamma)
                if result == 0:    #EDK_OK
                    # print("%.6f, %.6f, %.6f, %.6f, %.6f \n" % (thetaValue.value, alphaValue.value, low_betaValue.value, high_betaValue.value, gammaValue.value))
                    with open(str(sys.argv[1])+'_r.csv', 'a') as f:
                        f.write("%.6f,%.6f,%.6f,%.6f,%.6f \n" % (thetaValue.value, alphaValue.value, low_betaValue.value, high_betaValue.value, gammaValue.value))
                # else:
                #     print('lost data')

    elif state != 0x0600:
        print("Internal error in Emotiv Engine ! ")

    time_delta = datetime.now() - start_time
    if time_delta.total_seconds() >= secondsLimit:
        break

""" Close the recording """
# -------------------------------------------------------------------------
libEDK.IEE_EngineDisconnect()
libEDK.IEE_EmoStateFree(eState)
libEDK.IEE_EmoEngineEventFree(eEvent)
