import numpy as np
from numpy import genfromtxt
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import cross_val_score
import urllib.request  # this library is to access url
import time

import sys
import os
import platform
import time
import ctypes
from datetime import datetime

from array import *
from ctypes import *

from clean_classify import set_data, filter_slice, select_model

if sys.platform.startswith('win32'):
    import msvcrt
elif sys.platform.startswith('linux'):
    import atexit
    from select import select

from ctypes import *

# set connections and variables
n_channel = 14
times = 3
preds = []

def calibration(att, flag, stream):
    """ Set the calibration data files """
    if os.path.exists('./calibration_a.csv') and flag:
        with open('calibration_a.csv', 'w') as f:
            f.write("%.6f,%.6f,%.6f,%.6f,%.6f \n" % (stream[0],stream[1],stream[2],stream[3],stream[4]))
    if os.path.exists('./calibration_r.csv') and flag:
        with open('calibration_r.csv', 'w') as f:
            f.write("%.6f,%.6f,%.6f,%.6f,%.6f \n" % (stream[0],stream[1],stream[2],stream[3],stream[4]))
    else:
        if att:
            with open('calibration_a.csv', 'a') as f:
                f.write("%.6f,%.6f,%.6f,%.6f,%.6f \n" % (stream[0],stream[1],stream[2],stream[3],stream[4]))
        else:
            with open('calibration_r.csv', 'a') as f:
                f.write("%.6f,%.6f,%.6f,%.6f,%.6f \n" % (stream[0],stream[1],stream[2],stream[3],stream[4]))

def start(clf,stream):
    """ Filter and predict the stream based on the classifier """
    ch_r = np.arange(0,stream.shape[0])
    for i in range(n_channel):
        ch_r = np.where((ch_r % n_channel) == i, i , ch_r)
    stream = np.concatenate([stream,ch_r[:,np.newaxis]],axis=1)
    stream_filtered, _ = filter_slice(stream)
    if (stream_filtered.shape[0]) > 0:
        data = stream_filtered[:,:,1]
        pred = clf.predict(data)
        print(pred)
        # pred = np.min(pred)
        # if preds != []:
        #     if preds[-1]==0.0 and int(pred) == 1 and np.random.random_sample()<0.5:
        #         pred = 0.0
        preds.append(pred)
        return(pred)
    else: print('not pred')

def load():
    """ Train and store the best classifier """
    # load data
    relax = np.genfromtxt('relax123.csv',delimiter=',')[1:]
    attention = np.genfromtxt('attention123.csv',delimiter=',')[1:]
    # relax = np.genfromtxt('prova_r.csv',delimiter=',')[1:]
    # attention = np.genfromtxt('prova_a.csv',delimiter=',')[1:]
    calib_r = np.genfromtxt('calibration_a.csv',delimiter=',')
    calib_a = np.genfromtxt('calibration_r.csv',delimiter=',')
    #set data
    train,labels = set_data(relax,attention)
    val,lab_val = set_data(relaxV,attentionV)
    # set calibration
    calib,calib_lab = set_data(calib_r,calib_a)
    # load model
    l_new = 0.05
    clf = LinearDiscriminantAnalysis(solver='lsqr',shrinkage=0.5)
    # fit with calibration
    clf.fit(train, labels, True, calib, calib_lab, l_new)
    # fit without calibration
    # clf.fit(train, labels)
    return clf


def step_calibration():
    """ Perform calibration """
    time.sleep(0.01)
    stream = []
    calib = 0
    calibflag = True
    stoptime = 0
    att = True
    maxcalib = 200

    while (1):
        state = libEDK.IEE_EngineGetNextEvent(eEvent)
        if state == 0:
            eventType = libEDK.IEE_EmoEngineEventGetType(eEvent)
            libEDK.IEE_EmoEngineEventGetUserId(eEvent, user)
            if eventType == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
                ready = 1
                libEDK.IEE_FFTSetWindowingType(userID, 1);  # 1: libEDK.IEE_WindowingTypes_enum.IEE_HAMMING
            if ready == 1:
                for i in channelList:
                    result = c_int(0)
                    result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)
                    if result == 0:    #EDK_OK
                        stream.append([round(thetaValue.value,6),round(alphaValue.value,6),round(low_betaValue.value,6),round(high_betaValue.value,6), round(gammaValue.value,6)])
                        if calib < maxcalib:
                            if stoptime == 0:
                                print('CALIBRATION ATTENTION')
                                time.sleep(1)
                                stoptime += 1
                            calib += 1
                            calibration(att,calibflag,[round(thetaValue.value,6),round(alphaValue.value,6),round(low_betaValue.value,6),round(high_betaValue.value,6), round(gammaValue.value,6)])
                            calibflag = False
                        if calib >= maxcalib and calib<maxcalib*2:
                            if stoptime == 1:
                                print('CALIBRATION RELAX')
                                att = False
                                time.sleep(1)
                                stoptime += 1
                            calib += 1
                            calibration(att,calibflag,[round(thetaValue.value,6),round(alphaValue.value,6),round(low_betaValue.value,6),round(high_betaValue.value,6), round(gammaValue.value,6)])
                    # else: print(i)
                # print(len(stream))
                if calib >= maxcalib*2:
                    print('END CALIBRATION')
                    break

        elif state != 0x0600:
            print("Internal error in Emotiv Engine ! ")


def run():
    """ Run the test with the real time predictions """
    time.sleep(1)
    clf = load()
    stream = []
    start_time = datetime.now()

    while (1):
        state = libEDK.IEE_EngineGetNextEvent(eEvent)
        if state == 0:
            eventType = libEDK.IEE_EmoEngineEventGetType(eEvent)
            libEDK.IEE_EmoEngineEventGetUserId(eEvent, user)
            if eventType == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
                ready = 1
                libEDK.IEE_FFTSetWindowingType(userID, 1);  # 1: libEDK.IEE_WindowingTypes_enum.IEE_HAMMING
            if ready == 1:
                for i in channelList:
                    result = c_int(0)
                    result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)
                    if result == 0:    #EDK_OK
                        stream.append([round(thetaValue.value,6),round(alphaValue.value,6),round(low_betaValue.value,6),round(high_betaValue.value,6), round(gammaValue.value,6)])
                if len(stream) == (14*times):
                    stream = np.array(stream)
                    start(clf,stream)
                    stream = []
            else: print('lost stream')
        elif state != 0x0600:
            print("Internal error in Emotiv Engine ! ")

        time_delta = datetime.now() - start_time
        if time_delta.total_seconds() >= 1000:
            break

    # -------------------------------------------------------------------------
    libEDK.IEE_EngineDisconnect()
    libEDK.IEE_EmoStateFree(eState)
    libEDK.IEE_EmoEngineEventFree(eEvent)


def main():
    try:
        libEDK = cdll.LoadLibrary("../../../Emotiv SDK Premium Edition v3.3.3/EDK/x86/edk.dll")
    except Exception as e:
        print('Error: cannot load EDK lib:', e)
        exit()

    IEE_EmoEngineEventCreate = libEDK.IEE_EmoEngineEventCreate
    IEE_EmoEngineEventCreate.restype = c_void_p
    eEvent = IEE_EmoEngineEventCreate()

    IEE_EmoEngineEventGetEmoState = libEDK.IEE_EmoEngineEventGetEmoState
    IEE_EmoEngineEventGetEmoState.argtypes = [c_void_p, c_void_p]
    IEE_EmoEngineEventGetEmoState.restype = c_int

    IEE_EmoStateCreate = libEDK.IEE_EmoStateCreate
    IEE_EmoStateCreate.restype = c_void_p
    eState = IEE_EmoStateCreate()

    userID = c_uint(0)
    user   = pointer(userID)
    ready  = 0
    state  = c_int(0)

    alphaValue     = c_double(0)
    low_betaValue  = c_double(0)
    high_betaValue = c_double(0)
    gammaValue     = c_double(0)
    thetaValue     = c_double(0)

    alpha     = pointer(alphaValue)
    low_beta  = pointer(low_betaValue)
    high_beta = pointer(high_betaValue)
    gamma     = pointer(gammaValue)
    theta     = pointer(thetaValue)

    channelList = array('I',[*range(3,17)])

    # -------------------------------------------------------------------------
    print("===================================================================")
    print("Pizza without pineapple connecting to the headset.")
    print("===================================================================")

    # -------------------------------------------------------------------------
    if libEDK.IEE_EngineConnect(create_string_buffer(b"Emotiv Systems-5")) != 0:
        print("Emotiv Engine start up failed.")
        exit();

    step_calibration()
    run()

if __name__ == "__main__":
    main()
