import time
import sys
from datetime import datetime
import numpy as np

def main():
    if len(sys.argv) != 2:
        print('Usage: python take_data.py <NAME_FILE>')
        exit()

    i = 0
    foc = 0
    rel = 0

    with open(str(sys.argv[1])+'_r.csv', 'w') as f:
        f.write("Theta,Alpha,Low_beta,High_beta,Gamma \n")
    with open(str(sys.argv[1])+'_a.csv', 'w') as f:
        f.write("Theta,Alpha,Low_beta,High_beta,Gamma \n")

    # loop that breaks
    while(1):
        i += 1
        state = np.random.randint(2)
        if(foc>15 and state == 1 ): state = 0
        if(rel>15 and state == 0 ): state = 1
        if(rel>15 and foc>15): break
        if bool(state):
            print("Be focused for 4 seconds")
            time.sleep(0.5)
            exec(open('take_attention.py').read())
            foc += 1
        else:
            print("Be relaxed for 20 seconds")
            time.sleep(0.5)
            exec(open('take_relax.py').read())
            rel += 1
        time.sleep(4)

if __name__ == "__main__":
    main()
