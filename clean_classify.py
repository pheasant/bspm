import numpy as np
from numpy import genfromtxt
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import cross_val_score
from scipy.io import savemat
import sys

# select number of channels
n_channel = 14

def filter_slice(stream, thr=10):
    """ Filter the stream of data to remove high values.
    Input: stream of data and threshold (default: 10)
    Output: tensor filtered and the unraveled matrix
    """
    # finding the minimum lenght of blocks of channels
    minl = min([len([*filter(lambda x: x[5]==i,stream)]) for i in range(n_channel)])
    # create the tensor
    tensor = []
    for i in range(n_channel):
        tensor.append([*filter(lambda x: x[5]==i,stream)][:minl])
    tensor = np.swapaxes(tensor,0,1)
    # filter out the values
    minalpha = []
    for i,samples in enumerate(tensor):
        if samples[:,1].mean()<thr:
            minalpha.append(samples)
    minalpha = np.array(minalpha)
    # unravel the tensor to recreate the matrix
    mat_matlab = np.array([])
    if minalpha.shape[0]>0:
        mat_matlab = np.array(minalpha[0])
        for i in range(1,len(minalpha)):
            mat_matlab = np.concatenate([mat_matlab,minalpha[i]])
    return minalpha, mat_matlab

def set_data(relax,attention):
    """ Set the data to give to the classifier.
    Input: array of samples of relax and attention
    Output: data to classify and corresponding labels
    """
    # create channels' array and adding it to the data
    ch_r = np.arange(0,relax.shape[0])
    ch_a = np.arange(0,attention.shape[0])
    for i in range(n_channel):
        ch_r = np.where((ch_r % n_channel) == i, i , ch_r)
    for i in range(n_channel):
        ch_a = np.where((ch_a % n_channel) == i, i , ch_a)
    relax.shape,ch_r.shape
    relax = np.concatenate([relax,ch_r[:,np.newaxis]],axis=1)
    attention = np.concatenate([attention,ch_a[:,np.newaxis]],axis=1)
    # filter
    minalpha_r, mat_matlab_r = filter_slice(relax)
    minalpha_a, mat_matlab_a = filter_slice(attention)
    # taking just alpha channel
    final_a = minalpha_a[:,:,1]
    final_r = minalpha_r[:,:,1]
    # create labels
    lab_a = np.ones(shape=(final_a.shape[0],1))
    lab_r = np.zeros(shape=(final_r.shape[0],1))
    # create final data shape
    final_train = np.concatenate([final_a,final_r])
    final_label = np.concatenate([lab_a,lab_r])
    #### for matlab analysis
    # savemat('matlab/tot_relax_stream.mat',mdict={'train': mat_matlab_r})
    # savemat('matlab/tot_attention_stream.mat',mdict={'test': mat_matlab_a})
    # savemat('matlab/train_tutorial_stream.mat',mdict={'train': train})
    # savemat('matlab/test_tutoral_stream.mat',mdict={'test': test})
    ####
    return final_train,final_label


def select_model(relax,attention,relaxV,attentionV):
    """ Return the best model's cross validation score and test accuracy """
    # load test data
    relaxT = np.genfromtxt('data/experiment3/relax_new.csv',delimiter=',')[1:]
    attentionT = np.genfromtxt('data/experiment3/attention_new.csv',delimiter=',')[1:]
    # set data
    train,lab_tr = set_data(relax,attention)
    val,lab_val = set_data(relaxV,attentionV)
    test,lab_test = set_data(relaxT,attentionT)
    # set parameters
    shrinkages = [0.1,0.5,0.9]
    bestsh = 0.1
    bestscore = 0
    # model selection
    for sh in shrinkages:
        clf = LinearDiscriminantAnalysis(solver='lsqr',shrinkage=sh)
        clf.fit(train, lab_tr)
        scores = np.mean(cross_val_score(clf, val, lab_val, cv=5))
        if scores > bestscore:
            bestscore = scores
            bestsh = sh
    # test
    clf = LinearDiscriminantAnalysis(solver='lsqr',shrinkage=bestsh)
    clf.fit(train, lab_tr)
    pred = clf.predict(test)
    true_pos = np.sum([(int(x == lab_test[i])) for i,x in enumerate(pred)])
    acc = true_pos/len(pred)
    print("#################################################")
    print("best shrinkage:"+ str(bestsh))
    print("best validation score:"+ str(acc))
    print("best accuracy:"+ str(acc))
    print("#################################################")
    return clf

def main():
    """ Print the best model's cross validation score and test accuracy """
    relax = np.genfromtxt('data/experiment3/relax123.csv',delimiter=',')[1:]
    attention = np.genfromtxt('data/experiment3/attention123.csv',delimiter=',')[1:]
    relaxV = np.genfromtxt('data/experiment3/prova_r.csv',delimiter=',')[1:]
    attentionV = np.genfromtxt('data/experiment3/prova_a.csv',delimiter=',')[1:]
    select_model(relax, attention, relaxV, attentionV)
    print(sys.argv)


if __name__ == "__main__":
    main()
